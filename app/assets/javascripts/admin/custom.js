function load_posts() {
  $.ajax({
    url: "/admin/posts/events",
  data: {last_id: last_id},
  success: function(data) {
    var posts = JSON.parse(data["posts"]);
    for(var i = 0; i < posts.length; i++) {
      var html = "<tr id='post-" + posts[i]["id"] + "'>" +
    "<td>" + posts[i]["user_name"] + "</td>" +
    "<td>" + posts[i]["content"] + "</td>" +
    "<td><a href='#' class='approve-post' data-id='" + posts[i]["id"] + "'>Approve</a></td>" +
    "<td><a href='#' class='reject-post' data-id='" + posts[i]["id"] + "'>Reject</a></td>" +
    "<td>" + posts[i]["provider"] + "</td>" +
    "</tr>";
    $(".posts-table").append(html);
    last_id = posts[i]["id"];
    }
  },
  dataType: 'json'
  });
}
$(document).ready(function() {
  $(document.body).on('click', '.approve-post', function(e) {
    var link_id = $(this).data("id");
    e.preventDefault();
    $.ajax({
      url: "/admin/posts/" + link_id + "/approve",
      data: {},
      success: function(data) {
        var div = $("#post-" + link_id).remove();
      },
      error: function(data) {
             },
      dataType: "json"
    });
  });
  $(document.body).on('click', '.reject-post', function(e) {
    var link_id = $(this).data("id");
    e.preventDefault();
    $.ajax({
      url: "/admin/posts/" + link_id + "/reject",
      data: {},
      success: function(data) {
        var div = $("#post-" + link_id).remove();
      },
      error: function(data) {
             },
      dataType: "json"
    });
  });
  $("#update-factor").click(function(e) {
    e.preventDefault();
    var factor = $("#factor").val();
    $.ajax({
      url: "/admin/posts/update_factor",
      data: {factor: factor},
      method: "POST",
      success: function(data) {
        alert("Donation per post updated");
      },
      error: function(data) {
        alert("Donation per post could not be updated. Please try again.");
      },
      dataType: "json"
    });
  });
  setInterval('load_posts()', 20 * 1000);
});
