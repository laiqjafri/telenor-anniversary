class SubscriptionMailer < ApplicationMailer
  def reminder_email
    @url = "http://karomumkin.telenor.com.pk"
    @emails = Subscription.all.collect(&:email).uniq
    if @emails.count > 0
      mail(:to => '', :bcc => @emails.join(';'), :subject => "#KaroMumkin Telenor Campaign starting")
    end
  end
end
