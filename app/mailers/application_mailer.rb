class ApplicationMailer < ActionMailer::Base
  default from: "info@karomumkin.telenor.com.pk"
  layout 'mailer'
end
