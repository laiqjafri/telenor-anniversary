class TwitterStreamer

  def self.restart
    self.stop
    twitter_streamer = `cd #{APP_HOME} && rails runner -e $RAILS_ENV TwitterStreamer.start &`
    true
  end

  def self.stop
    res = `ps -ef | grep TwitterStreamer.start | grep -v grep | grep -v restart`
    if res.to_s.length > 0
      res = res.split("\n").collect{|s| s.squeeze(" ").strip}
      res.each do |process|
        process_num = process.split(" ")[1]
        `kill -9 #{process_num}`
      end
    end
  end

  def self.start
    client = Twitter::Streaming::Client.new do |config|
      config.consumer_key        = Rails.application.config.twitter_key
      config.consumer_secret     = Rails.application.config.twitter_secret
      config.access_token        = Rails.application.config.access_token
      config.access_token_secret = Rails.application.config.access_token_secret
    end

    # Use 'track' to track a list of single-word keywords
    keywords = ["#KaroMumkin"]
    if keywords.count > 0
      client.filter(:track => keywords.join(","), :stall_warnings => 'true') do |status|
        puts "#{status.user.name} @ #{status.created_at} -- #{status.text}"
        keywords.each do |keyword|
          if status.text =~ /#{keyword}/i
            post = Post.create :name => status.user.name,
              :content         => status.text,
              :user_name       => status.user.name,
              :screen_name     => status.user.screen_name,
              :profile_pic     => status.user.profile_image_uri,
              :provider        => 'twitter'
            puts "POST ==> #{post.inspect}"
          end
        end
      end
    end
  end
end
