require 'csv'

class Post < ActiveRecord::Base
  def self.csv_file
    CSV.generate do |csv|
      csv << ["ID", "Username", "Content", "When?", "Provider", "Approved"]
      Post.all.each do |p|
        csv << [p.id, p.screen_name, p.content.to_s.gsub("\n", " "), p.created_at.strftime("%d-%m-%Y"), p.provider, p.approved]
      end
    end
  end
end
