class DonationFactor < ActiveRecord::Base
  def self.donation_per_post
    d = DonationFactor.first
    unless d.present?
      d = DonationFactor.create :factor => 1
    end
    d
  end
end
