class AppStatus < ActiveRecord::Base
  def self.current_status
    app_status = first
    unless app_status.present?
      app_status = AppStatus.create :running => "inactive"
    end
    app_status
  end
end
