class Admin::PostsController < ApplicationController
  before_action :authenticate_admin!
  layout 'admin'
  def index
    @posts = Post.where("approved IS NULL")
  end

  def dashboard
  end

  def approved
    @posts = Post.where("approved = 1").paginate(:page => params[:page], :per_page => 25).order("created_at desc")
  end

  def rejected
    @posts = Post.where("approved = 0").paginate(:page => params[:page], :per_page => 25).order("created_at desc")
  end

  def events
    last_id = params[:last_id]
    posts = Post.where("id > #{last_id.to_i} AND approved IS NULL")
    respond_to do |format|
      format.json {render :json => {:posts => posts.to_json}}
    end
  end

  def approve
    post = Post.find_by_id params[:id]
    if post.present?
      post.update_attributes :approved => true, :approved_at => Time.now
    end
    respond_to do |format|
      format.json {render :json => {:success => true}.to_json}
    end
  end

  def reject
    post = Post.find_by_id params[:id]
    if post.present?
      post.update_attributes :approved => false
    end
    respond_to do |format|
      format.json {render :json => {:success => true}.to_json}
    end
  end

  def update_factor
    factor = DonationFactor.donation_per_post
    factor.update_attributes :factor => params[:factor].to_i
    respond_to do |format|
      format.json {render :json => {:success => true}.to_json}
    end
  end

  def subscriptions
    @subscriptions = Subscription.paginate(:per_page => 25, :page => params[:page])
  end

  def status
    AppStatus.current_status.update_attributes :running => params[:status]
    return redirect_to("/admin")
  end

  def reminder_email
    SubscriptionMailer.reminder_email.deliver
    return redirect_to("/admin")
  end

  def download
    respond_to do |format|
      format.csv {
        send_data Post.csv_file, :type => 'text/csv', :disposition => "attachment;filename=KaroMumkinPosts.csv"
      }
    end
  end
end
