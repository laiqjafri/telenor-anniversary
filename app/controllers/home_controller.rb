class HomeController < ApplicationController
  before_action :set_donation_factor
  def index
    if AppStatus.current_status.running == "active" or AppStatus.current_status.running == "paused"
      @posts = Post.where("approved = 1").limit(8).order("approved_at desc")
      @pics = Post.all.where("approved = 1").order("approved_at desc").collect{|post| [post.profile_pic, post.user_name]}.uniq
      @posts_count = Post.where("approved = 1").count
      @page = 1
    else
      return render(:action => "inactive")
    end
  end

  def inactive
  end

  private

  def set_donation_factor
    @donation_factor = DonationFactor.donation_per_post
  end
end
