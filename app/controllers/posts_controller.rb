class PostsController < ApplicationController
  before_action :authenticate_user!, :only => [:tweet]
  def show
    post = Post.find_by_id params[:id]
    respond_to do |format|
      format.json {render :json => {:story => post.content}.to_json}
    end
  end

  def approved
    @page = params[:page].to_i
    offset = (@page - 1) * 8
    @posts = Post.where("approved = 1").offset(offset).limit(8).order("approved_at desc")
    render :layout => false
  end

  def pics
    @pics = Post.all.where("approved = 1").order("approved_at desc").collect{|post| [post.profile_pic, post.user_name]}.uniq
    render :layout => false
  end

  def donation_raised
    @posts_count = Post.where("approved = 1").count
    respond_to do |format|
      format.json {render :json => {:amount => @posts_count * DonationFactor.donation_per_post.factor}.to_json}
    end
  end

  def tweet
    if current_user.tweet(params[:tweet])
      unless params[:tweet].to_s =~ /#KaroMumkin/i
        Post.create :name => current_user.twitter_identity.user_name,
          :content     => params[:tweet],
          :user_name   => current_user.twitter_identity.user_name,
          :screen_name => current_user.twitter_identity.screen_name,
          :profile_pic => current_user.twitter_identity.profile_pic,
          :provider    => 'twitter'
      end
      respond_to do |format|
        format.json {render :json => {:success =>  true}.to_json}
      end
    else
      respond_to do |format|
        format.json {render :json => {:success => false}.to_json}
      end
    end
  end

  def post_to_fb
    if user_signed_in? and current_user.facebook_identity.present?
      graph = Koala::Facebook::API.new(current_user.facebook_identity.oauth_token)
      post = graph.get_object(params[:post_id])
      Post.create :name => post["from"]["name"],
        :content     => post["message"],
        :user_name   => post["from"]["name"],
        :screen_name => post["from"]["name"],
        :profile_pic => current_user.facebook_identity.profile_pic,
        :provider    => 'facebook'
      respond_to do |format|
        format.json {render :json => {:success =>  true}.to_json}
      end
    end
  end

  def subscribe
    success = false
    message = ""
    subscription = Subscription.find_by :email => params[:email]
    if subscription.present?
      message = "You have already subscribed. Please wait until the campaign starts"
    else
      Subscription.create :email => params[:email], :name => params[:name]
      success = true
      message = "You have subscribed successfully. We will notify you when the campaign starts."
    end
    respond_to do |format|
      format.json {render :json => {:success =>  success, :message => message}.to_json}
    end
  end
end
