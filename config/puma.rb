directory "/home/telsvc/telenor-anniversary"
environment 'production'
workers 4
threads 1, 4096

bind 'tcp://127.0.0.1:9292'
pidfile "/home/telsvc/telenor-anniversary/tmp/puma/pid"
state_path "/home/telsvc/telenor-anniversary/tmp/puma/state"

activate_control_app
