class ChangeAppStatusColumn < ActiveRecord::Migration
  def change
    change_column :app_statuses, :running, :string
  end
end
