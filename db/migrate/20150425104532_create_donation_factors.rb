class CreateDonationFactors < ActiveRecord::Migration
  def change
    create_table :donation_factors do |t|
      t.integer :factor, :null => false, :default => 1
      t.timestamps null: false
    end
  end
end
