class CreatePosts < ActiveRecord::Migration
  def change
    create_table :posts do |t|
      t.string :content
      t.string :name
      t.string :user_name #Email in case of facebook
      t.string :screen_name
      t.string :profile_pic
      t.string :provider #twitter or facebook
      t.boolean :approved
      t.timestamps null: false
    end
    add_index :posts, :approved
  end
end
