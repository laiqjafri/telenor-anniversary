class CreateMessages < ActiveRecord::Migration
  def change
    create_table :messages do |t|
      t.text :content
      t.string :name
      t.string :user_name
      t.string :screen_name
      t.string :profile_pic
      t.string :provider
      t.timestamps null: false
    end
  end
end
