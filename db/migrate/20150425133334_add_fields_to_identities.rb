class AddFieldsToIdentities < ActiveRecord::Migration
  def change
    add_column :identities, :name, :string
    add_column :identities, :screen_name, :string
    add_column :identities, :user_name, :string
  end
end
